/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3;

/**
 *
 * @author Name
 */
public class findIndex {
    public static void main(String[] args) {
        int[] nums = {4,5,6,7,0,1,2};
        int target = 0;
        int result = findTarget(nums,target);
        System.out.println(result);

        int[] nums2 = {4,5,6,7,0,1,2};
        int target2 = 3;
        int result2 = findTarget(nums2,target2);
        System.out.println(result2);

        int[] nums3 = {1};
        int target3 = 0;
        int result3 = findTarget(nums3,target3);
        System.out.println(result3);
    }
    public static int findTarget(int[] nums,int target){
        int tar = 0;
        for(int i = 0 ; i < nums.length ; i++){
            if(nums[i] == target){
                tar = i;
                return tar;
            }
        }
        return -1;
    }
}
